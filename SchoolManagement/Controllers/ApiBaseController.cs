﻿using SchoolManagement.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Controllers
{
    public class ApiBaseController : ApiController
    {
        private UnitOfWork _unitOfWork;
        public UnitOfWork UnitOfWork => _unitOfWork ?? (_unitOfWork = new UnitOfWork());
    }
}