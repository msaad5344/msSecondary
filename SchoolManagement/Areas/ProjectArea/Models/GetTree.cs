﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement
{
    public class GetTree
    {
        private string _config;
        public GetTree(string conectionString)
        {
            _config = conectionString;
        }
        public List<TreeModel> GetAllTree()
        {
            string qury = "select clt_id,clt_ArName,clt_EnName from [dbo].[Clients];";
            //var read = GetItem(qury);
            var model = new List<TreeModel>();
            using (var con = new SqlConnection(_config))
            {
                SqlCommand com = new SqlCommand(qury, con);
                con.Open();
                using (var read = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (read.Read())
                    {
                        model.Add(new TreeModel
                        {
                            Cnt_id = (int)read["clt_id"],
                            Cnt_code = read["clt_id"].ToString(),
                            Cnt_name = read["clt_ArName"].ToString(),
                            Cnt_ENname = read["clt_EnName"].ToString(),
                            EntityModels = GetAllEntity((int)read["clt_id"])
                        });
                    }
                }
            }

            return model;
        }
        public List<EntityModel> GetAllEntity(int id)
        {
            string qury = "select ent_code,ent_id,ent_ENName,ent_ArName from [dbo].[client_Entity] where clt_Id=" + id;
            //var read = GetItem(qury);
            var model = new List<EntityModel>();
           // if (read != null)
            
            using (var con = new SqlConnection(_config))
            {
                SqlCommand com = new SqlCommand(qury, con);
                con.Open();
                using (var read = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (read.Read())
                    {
                        model.Add(new EntityModel
                        {
                            Ent_id = (int)read["ent_id"],
                            Ent_code = read["ent_code"].ToString(),
                            Ent_name = read["ent_ArName"].ToString(),
                            Ent_ENname = read["ent_ENName"].ToString(),
                            DivisionModels = GetAllDivission((int)read["ent_id"])
                        });
                    }
                    }
            }

            return model;
        }
        public List<DivisionModel> GetAllDivission(int id)
        {
            string qury = "select div_id,div_code,div_ArName,div_ENName from [dbo].[Division] where ent_id=" + id;
            //var read = GetItem(qury);
            var model = new List<DivisionModel>();
            //if (read != null)
             

            using (var con = new SqlConnection(_config))
            {
                SqlCommand com = new SqlCommand(qury, con);
                con.Open();
                using (var read = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (read.Read())
                    {
                        model.Add(new DivisionModel
                        {
                            Div_id = (int)read["div_id"],
                            Div_code = read["div_code"].ToString(),
                            Div_name = read["div_ArName"].ToString(),
                            Div_ENname = read["div_ENName"].ToString(),
                            CourseModels = GetAllCourses((int)read["div_id"])
                        });
                    }
                    }
            }
            return model;
        }
        public List<CourseModel> GetAllCourses(int id)
        {
            string qury = "select crs_id,crs_code,crs_ArName,crs_EnName from [dbo].[Courses] where div_code=" + id;
           // var read = GetItem(qury);
            var model = new List<CourseModel>();
           // if (read != null)
              

            using (var con = new SqlConnection(_config))
            {
                SqlCommand com = new SqlCommand(qury, con);
                con.Open();
                using (var read = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (read.Read())
                    {
                        model.Add(new CourseModel
                        {
                            Crs_id = (int)read["crs_id"],
                            Crs_code = read["crs_code"].ToString(),
                            Crs_name = read["crs_ArName"].ToString(),
                            Crs_ENname = read["crs_EnName"].ToString(),
                            BatchModels = GetAllBatches((int)read["crs_id"])
                        });
                    }
                    }
            }

            return model;
        }
        public List<BatchModel> GetAllBatches(int id)
        {
            string qury = "select btc_id,btc_code,btc_ArName,btc_EnName from [dbo].[Batches] where crs_id=" + id;
            //var read = GetItem(qury);
            var model = new List<BatchModel>();
            using (var con = new SqlConnection(_config))
            {
                SqlCommand com = new SqlCommand(qury, con);
                con.Open();
                using (var read = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                        while (read.Read())
                        {
                            model.Add(new BatchModel
                            {
                               Btc_id = (int)read["btc_id"],
                               Btc_code = read["btc_code"].ToString(),
                               Btc_name = read["btc_ArName"].ToString(),
                               Btc_ENname = read["btc_EnName"].ToString()
                            });
                        }
                }
            }
         
            return model;
        }


    }
}
