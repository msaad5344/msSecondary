﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Areas.ProjectArea.Models
{
    public class StudentVm
    {
        public int Id { get; set; }
        public int Name { get; set; }
        public List<StudentDetailVm> DetailList { get; set; }
    }
    public class StudentDetailVm
    {
        public int Id { get; set; }
        public int Name { get; set; }
        public List<xDetailVm> XdetailList { get; set; }
    }
    public class xDetailVm
    {
        public int Id { get; set; }
        public int Name { get; set; }
        public bool Added { get; set; }
        public bool Deleted { get; set; }
        public bool Edited { get; set; }
    }
}