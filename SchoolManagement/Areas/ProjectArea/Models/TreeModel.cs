﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement
{
    public class TreeModel
    {
        public int Cnt_id { get; set; }
        public string Cnt_name { get; set; }
        public string Cnt_ENname { get; set; }
        public string Cnt_code { get; set; }
        public List<EntityModel> EntityModels { get; set; }
    }

    public class EntityModel
    {
        public int Ent_id { get; set; }
        public string Ent_name { get; set; }
        public string Ent_ENname { get; set; }
        public string Ent_code { get; set; }
        public List<DivisionModel> DivisionModels { get; set; }
    }

    public class DivisionModel
    {
        public int Div_id { get; set; }
        public string Div_name { get; set; }
        public string Div_ENname { get; set; }
        public string Div_code { get; set; }
        public List<CourseModel> CourseModels { get; set; }
    }

    public class CourseModel
    {
        public int Crs_id { get; set; }
        public string Crs_name { get; set; }
        public string Crs_ENname { get; set; }
        public string Crs_code { get; set; }
        public List<BatchModel> BatchModels { get; set; }
    }

    public class BatchModel
    {
        public int Btc_id { get; set; }
        public string Btc_name { get; set; }
        public string Btc_ENname { get; set; }
        public string Btc_code { get; set; }
    }
}
