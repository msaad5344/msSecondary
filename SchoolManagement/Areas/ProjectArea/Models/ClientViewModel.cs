﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Areas.ProjectArea.Models
{
    public class ClientViewModel
    {
        public int clt_id { get; set; }
        public string clt_ArName { get; set; }
        public string clt_EnName { get; set; }
        public string clt_address1 { get; set; }
        public string clt_address2 { get; set; }
        public string clt_phone { get; set; }
        public string clt_email { get; set; }
        public string clt_website { get; set; }
        public string clt_logo { get; set; }
        public string clt_CR { get; set; }
        public string clt_Tax { get; set; }
        public string clt_licNo { get; set; }
    }
}