﻿using SchoolManagement.Areas.ProjectArea.Models;
using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by ashraf 24-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/Entity")]
    public class EntityController : ApiBaseController
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    

        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Entitys.Find(id);
                var Entity = new 
                { ent_website = result.ent_website,
                    ent_Tax = result.ent_Tax,
                    ent_address1 = result.ent_address1,
                    ent_address2 = result.ent_address2,
                    ent_ArName = result.ent_ArName,
                    ent_CR = result.ent_CR,
                    ent_email=result.ent_email,
                    ent_ENName=result.ent_ENName,
                    ent_id=result.ent_id,
                    ent_licNo=result.ent_licNo,
                    ent_logo=result.ent_logo,
                    ent_phone=result.ent_phone,
                    clt_code = result.clt_code,
                    ent_code = result.ent_code
                };
                return Ok(Entity);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] client_Entity model)
        {

            UnitOfWork.Entitys.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok(model);
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]client_Entity model)
        {
            var mod = UnitOfWork.Entitys.Find(model.ent_id);
            mod.ent_address1 = model.ent_address1;
            mod.ent_address2 = model.ent_address2;
            mod.ent_ArName = model.ent_ArName;
            mod.ent_CR = model.ent_CR;
            mod.ent_email = model.ent_email;
            mod.ent_ENName = model.ent_ENName;
            mod.ent_licNo = model.ent_licNo;
            mod.ent_phone = model.ent_phone;
            mod.ent_Tax = model.ent_Tax;
            mod.ent_website = model.ent_website;
            mod.ent_code = model.ent_code;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok(model);
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Entitys.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here

        [HttpPost]
        [Route("SaveEntityPhoto")]
        public IHttpActionResult SaveEntityPhoto()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                       HttpContext.Current.Request.Files[0] : null;

            if (Path.GetExtension(file.FileName).ToLower() != ".jpg"
           && Path.GetExtension(file.FileName).ToLower() != ".png"
           && Path.GetExtension(file.FileName).ToLower() != ".gif"
           && Path.GetExtension(file.FileName).ToLower() != ".jpeg")
            {
                return Content(HttpStatusCode.BadRequest, "Error in formate");
            }
            string logo = "";
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    var id = Convert.ToInt32(HttpContext.Current.Request.Form["Id"]);
                    var name = id + Path.GetExtension(file.FileName).ToLower();
                    var path = Path.Combine(
                          HttpContext.Current.Server.MapPath("~/imgs/entity"),
                          name
                      );
                    var pp = HttpContext.Current.Server.MapPath("~/imgs/entity/") + name;
                    if (File.Exists(pp))
                    {
                        File.Delete(pp + name);
                    }
                    logo = @"/imgs/entity/" + name;
                    try
                    {
                        var clnt = UnitOfWork.Entitys.Find(id);
                        if (clnt != null)
                        {
                            file.SaveAs(path);
                            clnt.ent_logo = logo;
                            UnitOfWork.Complete();
                        }
                    }
                    catch (Exception ) { }
                }
                catch (Exception ) { }
            }
            return Ok(logo);
        }
    }
}
