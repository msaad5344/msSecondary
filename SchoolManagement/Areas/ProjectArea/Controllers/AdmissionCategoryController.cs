﻿using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by Ashraf 23-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/AdmissionCategory")]
    public class AdmissionCategoryController : ApiBaseController
    {
        //--------------------------------GetAll----------------------------------------------
        [HttpGet]
        [Route("GetAll")]
        //getting all AdmissionCategory data by creating annonymous object "same as View model"
        public IHttpActionResult GetAll()
        {
            var result = UnitOfWork.AdmissionCategory.GetAll().Select(x => new
            {
                Id = x.admc_id,
                NameAr = x.admc_arName,
                NameEn = x.admc_enName
            });
            return Ok(result);
        }
        //------------------------------------------------------------------------------

        //---------------------------Get specific student----------------------------------
        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.AdmissionCategory.Find(id);
                return Ok(result);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] AdmissionCategory model)
        {

            UnitOfWork.AdmissionCategory.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok();
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]AdmissionCategory model)
        {
            var student = UnitOfWork.AdmissionCategory.Find(model.admc_id);
            student.admc_enName = model.admc_enName;
            student.admc_arName = model.admc_arName;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok();
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.AdmissionCategory.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here
    }
}
