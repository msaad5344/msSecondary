﻿using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by Ashraf 23-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/AdmissionAssStatus")]
    public class AdmissionAssStatusController : ApiBaseController
    {
        //--------------------------------GetAll----------------------------------------------
        [HttpGet]
        [Route("GetAll")]
        //getting all AdminAssStatus data by creating annonymous object "same as View model"
        public IHttpActionResult GetAll()
        {
            var result = UnitOfWork.AdminAssStatus.GetAll().Select(x => new
            {
                Id = x.adas_id,
                NameAr = x.adas_arName,
                NameEn = x.adas_enName
            });
            return Ok(result);
        }
        //------------------------------------------------------------------------------

        //---------------------------Get specific student----------------------------------
        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.AdminAssStatus.Find(id);
                return Ok(result);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] AdminAssStatus_IDs model)
        {

            UnitOfWork.AdminAssStatus.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok();
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]AdminAssStatus_IDs model)
        {
            var student = UnitOfWork.AdminAssStatus.Find(model.adas_id);
            student.adas_enName = model.adas_enName;
            student.adas_arName = model.adas_arName;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok();
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.AdminAssStatus.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here
    }
}
