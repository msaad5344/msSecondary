﻿using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by MohamedSaad 22-5-2017
    //we should use Git or tfs in developing "ms note"


        //Note Controller inheret from the APIBaseController 
    [RoutePrefix("api/Student")]
    public class StudentsController : ApiBaseController
    {
        //--------------------------------GetAll----------------------------------------------
        [HttpGet]
        [Route("GetAll")]
        //getting all students data by creating annonymous object "same as View model"
        public IHttpActionResult GetAll()
        {
            var result = UnitOfWork.Students.GetAll().Select(x => new
            {
                stf_id = x.stf_id,
                stf_arName = x.stf_arName,
                stf_enName = x.stf_enName
            });
            return Ok(result);
        }
        //------------------------------------------------------------------------------

        //---------------------------Get specific student----------------------------------
        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Students.Find(id);
                return Ok(result);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] StudentForm model)
        {
            StudentForm newStudent = new StudentForm
            {
                stf_id = model.stf_id,
                stf_enName = model.stf_enName,
                stf_arName = model.stf_arName
            };
            UnitOfWork.Students.Add(newStudent);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok();
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]StudentForm model)
        {
            var student = UnitOfWork.Students.Find(model.stf_id);
            student.stf_enName = model.stf_enName;
            student.stf_arName = model.stf_arName;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok();
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Students.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here
    }
}
