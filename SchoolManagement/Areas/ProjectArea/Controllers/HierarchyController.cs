﻿using SchoolManagement.Areas.ProjectArea.Models;
using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by ashraf 24-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/Hierarchy")]
    public class HierarchyController : ApiBaseController
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //--------------------------------GetAll----------------------------------------------
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            //class made by using normal sql query return all tree branches
            var hierachy = new GetTree(ConnectionString);
            var tree = hierachy.GetAllTree();
            return Ok(tree);
        }
   


        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Clients.Find(id);
                var client = new ClientViewModel
                { clt_website = result.clt_website,
                    clt_Tax = result.clt_Tax,
                    clt_address1 = result.clt_address1,
                    clt_address2 = result.clt_address2,
                    clt_ArName = result.clt_ArName,
                    clt_CR = result.clt_CR,
                    clt_email=result.clt_email,
                    clt_EnName=result.clt_EnName,
                    clt_id=result.clt_id,
                    clt_licNo=result.clt_licNo,
                    clt_logo=result.clt_logo,
                    clt_phone=result.clt_phone
                };
                return Ok(client);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] Client model)
        {

            UnitOfWork.Clients.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok();
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]Client model)
        {
            var mod = UnitOfWork.Clients.Find(model.clt_id);
            mod.clt_address1 = model.clt_address1;
            mod.clt_address2 = model.clt_address2;
            mod.clt_ArName = model.clt_ArName;
            mod.clt_CR = model.clt_CR;
            mod.clt_email = model.clt_email;
            mod.clt_EnName = model.clt_EnName;
            mod.clt_licNo = model.clt_licNo;
            mod.clt_phone = model.clt_phone;
            mod.clt_Tax = model.clt_Tax;
            mod.clt_website = model.clt_website;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok();
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Clients.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here

        [HttpPost]
        [Route("SaveClientPhoto")]
        public IHttpActionResult SaveEntityPhoto()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                       HttpContext.Current.Request.Files[0] : null;

            if (Path.GetExtension(file.FileName).ToLower() != ".jpg"
           && Path.GetExtension(file.FileName).ToLower() != ".png"
           && Path.GetExtension(file.FileName).ToLower() != ".gif"
           && Path.GetExtension(file.FileName).ToLower() != ".jpeg")
            {
                return Content(HttpStatusCode.BadRequest, "Error in formate");
            }
            string logo = "";
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    var id = Convert.ToInt32(HttpContext.Current.Request.Form["Id"]);
                    var name = id + Path.GetExtension(file.FileName).ToLower();
                    var path = Path.Combine(
                          HttpContext.Current.Server.MapPath("~/imgs/client"),
                          name
                      );
                    var pp = HttpContext.Current.Server.MapPath("~/imgs/client/") + name;
                    if (File.Exists(pp))
                    {
                        File.Delete(pp + name);
                    }
                    logo = @"/imgs/client/" + name;
                    try
                    {
                        var clnt = UnitOfWork.Clients.Find(id);
                        if (clnt != null)
                        {
                            file.SaveAs(path);
                            clnt.clt_logo = logo;
                            UnitOfWork.Complete();
                        }
                    }
                    catch (Exception ) { }
                }
                catch (Exception ) { }
            }
            return Ok(logo);
        }
    }
}
