﻿using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by Ashraf 23-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/AdmissionAssType")]
    public class AdmissionAssTypeController : ApiBaseController
    {
        //--------------------------------GetAll----------------------------------------------
        [HttpGet]
        [Route("GetAll")]
        //getting all AdmissionAssType data by creating annonymous object "same as View model"
        public IHttpActionResult GetAll()
        {
            var result = UnitOfWork.AdmissionAssType.GetAll().Select(x => new
            {
                Id = x.adat_id,
                NameAr = x.adat__arName,
                NameEn = x.adat__enName
            });
            return Ok(result);
        }
        //------------------------------------------------------------------------------

        //---------------------------Get specific student----------------------------------
        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.AdmissionAssType.Find(id);
                return Ok(result);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] AdminAssTypes_IDs model)
        {

            UnitOfWork.AdmissionAssType.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok();
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]AdminAssTypes_IDs model)
        {
            var student = UnitOfWork.AdmissionAssType.Find(model.adat_id);
            student.adat__enName = model.adat__enName;
            student.adat__arName = model.adat__arName;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok();
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.AdmissionAssType.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here
    }
}
