﻿using SchoolManagement.Areas.ProjectArea.Models;
using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by ashraf 24-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/Courses")]
    public class CoursesController : ApiBaseController
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
   


        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Courses.Find(id);
                var Cours = new 
                { 
                    crs_ArName = result.crs_ArName,
                    crs_EnName=result.crs_EnName,
                    crs_id=result.crs_id,
                    div_code = result.div_code,
                    crs_code = result.crs_code,
                };
                return Ok(Cours);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] Cours model)
        {

            UnitOfWork.Courses.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok(model);
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]Cours model)
        {
            var mod = UnitOfWork.Courses.Find(model.crs_id);
            mod.crs_code = model.crs_code;
            mod.div_code = model.div_code;
            mod.crs_ArName = model.crs_ArName;
            mod.crs_EnName = model.crs_EnName;
          
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok(model);
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Courses.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------
        
    }
}
