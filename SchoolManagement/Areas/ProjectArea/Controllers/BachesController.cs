﻿using SchoolManagement.Areas.ProjectArea.Models;
using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by ashraf 24-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/Baches")]
    public class BachesController : ApiBaseController
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
   


        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Baches.Find(id);
                var Batch = new 
                { 
                    btc_ArName = result.btc_ArName,
                    btc_EnName=result.btc_EnName,
                    btc_id=result.btc_id,
                    crs_code = result.crs_code,
                    btc_code = result.btc_code,
                    crs_id=result.crs_id

                };
                return Ok(Batch);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] Batch model)
        {

            UnitOfWork.Baches.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok(model);
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]Batch model)
        {
            var mod = UnitOfWork.Baches.Find(model.btc_id);
            mod.btc_code = model.btc_code;
            mod.crs_code = model.crs_code;
            mod.btc_ArName = model.btc_ArName;
            mod.btc_EnName = model.btc_EnName;
          
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok(model);
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Baches.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------
        
    }
}
