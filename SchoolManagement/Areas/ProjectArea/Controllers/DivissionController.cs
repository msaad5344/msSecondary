﻿using SchoolManagement.Areas.ProjectArea.Models;
using SchoolManagement.Controllers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SchoolManagement.Areas.ProjectArea.Controllers
{
    //created by ashraf 24-5-2017
    //we should use Git or tfs in developing "ms note"

    [RoutePrefix("api/Divission")]
    public class DivissionController : ApiBaseController
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
       

        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = UnitOfWork.Divisions.Find(id);
                var Division = new 
                { div_website = result.div_website,
                    div_Tax = result.div_Tax,
                    div_address1 = result.div_address1,
                    div_address2 = result.div_address2,
                    div_ArName = result.div_ArName,
                    div_CR = result.div_CR,
                    div_email=result.div_email,
                    div_ENName = result.div_ENName,
                    div_id=result.div_id,
                    div_licNo=result.div_licNo,
                    div_logo=result.div_logo,
                    div_phone=result.div_phone,
                    ent_code=result.ent_code,
                    div_code=result.div_code,
                    ent_id=result.ent_id
                };
                return Ok(Division);
            }
            catch
            {
                return BadRequest();
            }
        }
        //------------------------------------------------------------------------------

        //---------------------------Add new student----------------------------------
        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post([FromBody] Division model)
        {

            UnitOfWork.Divisions.Add(model);
            if (UnitOfWork.Complete() > 0)
            {
                return Ok(model);
            }
            return BadRequest();
        }
        //------------------------------------------------------------------------------

        //---------------------------editing new student----------------------------------
        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]Division model)
        {
            var mod = UnitOfWork.Divisions.Find(model.div_id);
            mod.div_address1 = model.div_address1;
            mod.div_address2 = model.div_address2;
            mod.div_ArName = model.div_ArName;
            mod.div_CR = model.div_CR;
            mod.div_email = model.div_email;
            mod.div_ENName = model.div_ENName;
            mod.div_licNo = model.div_licNo;
            mod.div_phone = model.div_phone;
            mod.div_Tax = model.div_Tax;
            mod.div_website = model.div_website;
            if (UnitOfWork.Complete() >= 0)
            {
                return Ok(model);
            }

            return BadRequest();
        }
        //------------------------------------------------------------------------------
        //---------------------------deleting the student-------------------------------
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                UnitOfWork.Divisions.Remove(id);
                if (UnitOfWork.Complete() > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //------------------------------------------------------------------------------

        //you can add your api methods here

        [HttpPost]
        [Route("SaveDivisionPhoto")]
        public IHttpActionResult SaveDivisionPhoto()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                       HttpContext.Current.Request.Files[0] : null;

            if (Path.GetExtension(file.FileName).ToLower() != ".jpg"
           && Path.GetExtension(file.FileName).ToLower() != ".png"
           && Path.GetExtension(file.FileName).ToLower() != ".gif"
           && Path.GetExtension(file.FileName).ToLower() != ".jpeg")
            {
                return Content(HttpStatusCode.BadRequest, "Error in formate");
            }
            string logo = "";
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    var id = Convert.ToInt32(HttpContext.Current.Request.Form["Id"]);
                    var name = id + Path.GetExtension(file.FileName).ToLower();
                    var path = Path.Combine(
                          HttpContext.Current.Server.MapPath("~/imgs/devision"),
                          name
                      );
                    var pp = HttpContext.Current.Server.MapPath("~/imgs/devision/") + name;
                    if (File.Exists(pp))
                    {
                        File.Delete(pp + name);
                    }
                    logo = @"/imgs/devision/" + name;
                    try
                    {
                        var clnt = UnitOfWork.Divisions.Find(id);
                        if (clnt != null)
                        {
                            file.SaveAs(path);
                            clnt.div_logo = logo;
                            UnitOfWork.Complete();
                        }
                    }
                    catch (Exception ) { }
                }
                catch (Exception ) { }
            }
            return Ok(logo);
        }
    }
}
