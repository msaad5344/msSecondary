//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolManagement.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Student03_StudentCourse
    {
        public int stu03_id { get; set; }
        public Nullable<int> stu00_id { get; set; }
        public Nullable<int> sy_id { get; set; }
        public Nullable<int> crs_id { get; set; }
        public Nullable<int> btc_id { get; set; }
        public string stu03_status { get; set; }
    
        public virtual Batch Batch { get; set; }
        public virtual Cours Cours { get; set; }
        public virtual SchoolYear SchoolYear { get; set; }
        public virtual Student00_Personnelnfo Student00_Personnelnfo { get; set; }
    }
}
