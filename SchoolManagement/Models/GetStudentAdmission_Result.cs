//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolManagement.Models
{
    using System;
    
    public partial class GetStudentAdmission_Result
    {
        public int stu02_id { get; set; }
        public Nullable<int> stu00_id { get; set; }
        public string stu02_admNo { get; set; }
        public Nullable<System.DateTime> stu02_admDate { get; set; }
        public Nullable<int> stu02_admCourse { get; set; }
        public Nullable<int> stu02_admbatch { get; set; }
        public Nullable<int> stu02_admyear { get; set; }
        public Nullable<int> admt_id { get; set; }
        public Nullable<int> stu00_Category { get; set; }
        public Nullable<bool> stu02_assrec { get; set; }
        public Nullable<System.DateTime> stu02_date { get; set; }
        public Nullable<int> stu02_assby { get; set; }
        public Nullable<int> adas_id { get; set; }
        public string stu02_assnotes { get; set; }
        public string btc_ArName { get; set; }
        public string btc_EnName { get; set; }
        public string crs_ArName { get; set; }
        public string crs_EnName { get; set; }
        public string sy_ArName { get; set; }
        public string sy_ENName { get; set; }
        public string admc_arName { get; set; }
        public string admc_enName { get; set; }
        public string adas_arName { get; set; }
        public string adas_enName { get; set; }
        public string admt_arName { get; set; }
        public string admt_enName { get; set; }
    }
}
