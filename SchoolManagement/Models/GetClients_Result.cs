//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolManagement.Models
{
    using System;
    
    public partial class GetClients_Result
    {
        public int clt_id { get; set; }
        public string clt_ArName { get; set; }
        public string clt_EnName { get; set; }
        public string clt_address1 { get; set; }
        public string clt_address2 { get; set; }
        public string clt_phone { get; set; }
        public string clt_email { get; set; }
        public string clt_website { get; set; }
        public string clt_logo { get; set; }
        public string clt_CR { get; set; }
        public string clt_Tax { get; set; }
        public string clt_licNo { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> updated_by { get; set; }
    }
}
