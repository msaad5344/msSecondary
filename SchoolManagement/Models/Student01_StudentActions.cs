//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolManagement.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Student01_StudentActions
    {
        public int stu01_id { get; set; }
        public Nullable<int> stu00_id { get; set; }
        public Nullable<int> stda_id { get; set; }
        public Nullable<System.DateTime> stu01_date { get; set; }
        public string stu01_note { get; set; }
    
        public virtual stdActions_StudentActions stdActions_StudentActions { get; set; }
        public virtual Student00_Personnelnfo Student00_Personnelnfo { get; set; }
    }
}
