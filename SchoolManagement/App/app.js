﻿(function () {
    // Define all your modules with no dependencies
    angular.module("AcademicModule", []);
    angular.module("AdmissionModule", []);


    var app = angular.module("App", ["ngRoute", "ui.bootstrap", "ui-notification", 'angularUtils.directives.dirPagination','ngBootbox',
                                     "AcademicModule", "AdmissionModule"])
        .config(function (NotificationProvider) {
            NotificationProvider.setOptions({
                delay: 10000,
                startTop: 40,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'bottom'
            });
        });



    //this method for initializing any data on the rootscope
    app.run(['$rootScope', function ($rootScope) {
    }])


    .config([
        '$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
            $locationProvider.hashPrefix('');
            $routeProvider
                .when('/', {
                    templateUrl: '/App/Modules/Academics/Dashboard/index.html',
                    controller: "AcademicDashBoardController"
                })
                .when('#', {
                    templateUrl: '/App/Modules/Academics/Dashboard/index.html'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
    ]);

}());