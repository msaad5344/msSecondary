﻿(function () {

    var StudentSvc = function ($http, $q) {

        var getAll = function () {
            var deferred = $q.defer();
            $http.get("api/Student/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
 
        var get = function (id) {
            var deferred = $q.defer();
            $http.get("/api/Student/Get/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };


        var add = function (model) {
            var deferred = $q.defer();
            $http.post("/api/Student/Post", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var update = function (model) {
            var deferred = $q.defer();
            $http.put("/api/Student/Update", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var remove = function (id) {
            var deferred = $q.defer();
            $http.delete("/api/Student/Delete/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
        }
    };

    var module = angular.module("AcademicModule");
    module.factory("StudentSvc", ["$http", "$q", StudentSvc]);

}());