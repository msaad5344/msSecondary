﻿(function () {

    var module = angular.module("AcademicModule", []);

    module.config([
        '$routeProvider', function ($routeProvider) {
            $routeProvider
                    .when("/students", {
                        templateUrl: "/App/Modules/Academics/Students/List.html",
                        controller: "StoresListController"
                    })
                    .when("/studentAdd", {
                        templateUrl: "/App/Modules/Academics/Students/Add.html",
                        controller: "StudentController"
                    })
                    .when("/student/:id", {
                        templateUrl: "/App/Modules/Academics/Students/Add.html",
                        controller: "StudentController"
                    });
        }
    ]);
}());