﻿(function () {

    var admissionCategoryController = function ($rootScope, $scope, $routeParams, $route, $ngBootbox, Notification, admissionCategorySvc) {

        //-----------------Mohamed Saad Comment---Don't remove it-------------------------------------------------------------

        //thease comments is for the design pattern of separating the ListView from the Add and Update
        //put Your Global varaibles here
        //examble ==> $scope.data = [];

        //-------------------------------------------------------------------------------------------------------------------

        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.save = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                admissionCategorySvc.add($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        $scope.model = {
                        };
                        Notification.success("تم الحفظ");
                        $scope.getAlladmissionCategorys();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                admissionCategorySvc.update($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAlladmissionCategorys();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }
        //-------------------------------------------------------------------------------------------------------------------

        //-----------------initializing the list data-------------------------------------------------------------------------
        $scope.getAlladmissionCategorys = function () {
            admissionCategorySvc.getAll().then(function (data) {
                $scope.admissionCategorysList = data.data;
                console.log(data);
            }, function (error) {
                console.log(error.data);
            });
        }
        $scope.getAlladmissionCategorys();
        //-------------------------------------------------------------------------------------------------------------------
        $scope.getItem = function (id) {
            admissionCategorySvc.get(id).then(function (response) {
                $scope.saveText = "Update";
                $scope.isAdd = false;
                $scope.model = response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //---------------------------------------------delete admissionCategory-------------------------------------------------------
        $scope.delete = function (id) {
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                admissionCategorySvc.remove(id).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAlladmissionCategorys();
                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //-------------------------------------------------------------------------------------------------------------------
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newItem = function () {
            $scope.redefineData();
        }
        //-------------------------------------------------------------------------------------------------------------------

        $scope.redefineData = function () {
            $scope.isAdd = true;
            $scope.saveText = "Save";
            $scope.model = {
                id: 0
            }
        }
        $scope.redefineData();

    }
    var module = angular.module("AcademicModule");
    module.controller("admissionCategoryController", ["$rootScope", "$scope", "$routeParams", "$route", "$ngBootbox", "Notification", "admissionCategorySvc", admissionCategoryController]);
}());