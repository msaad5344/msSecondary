﻿(function () {

    var studentsFormSvc = function ($http, $q) {

        var getAll = function () {
            var deferred = $q.defer();
            $http.get("api/StudentsForm/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
 
        var get = function (id) {
            var deferred = $q.defer();
            $http.get("/api/StudentsForm/Get/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };


        var add = function (model) {
            var deferred = $q.defer();
            $http.post("/api/StudentsForm/Post", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var update = function (model) {
            var deferred = $q.defer();
            $http.put("/api/StudentsForm/Update", model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var remove = function (id) {
            var deferred = $q.defer();
            $http.delete("/api/StudentsForm/Delete/" + id).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove
        }
    };

    var module = angular.module("AcademicModule");
    module.factory("studentsFormSvc", ["$http", "$q", studentsFormSvc]);

}());