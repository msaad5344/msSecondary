﻿(function () {

    var studentsFormController = function ($rootScope, $scope, $routeParams, $route, $ngBootbox, Notification, studentsFormSvc) {

        //-----------------Mohamed Saad Comment---Don't remove it-------------------------------------------------------------

        //thease comments is for the design pattern of separating the ListView from the Add and Update
        //put Your Global varaibles here
        //examble ==> $scope.data = [];

        //-------------------------------------------------------------------------------------------------------------------

        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.save = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                studentsFormSvc.add($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        $scope.model = {
                        };
                        Notification.success("تم الحفظ");
                        $scope.getAllstudentsForms();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                studentsFormSvc.update($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllstudentsForms();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }
        //-------------------------------------------------------------------------------------------------------------------

        //-----------------initializing the list data-------------------------------------------------------------------------
        $scope.getAllstudentsForms = function () {
            studentsFormSvc.getAll().then(function (data) {
                $scope.studentsFormsList = data.data;
                console.log(data);
            }, function (error) {
                console.log(error.data);
            });
        }
        $scope.getAllstudentsForms();
        //-------------------------------------------------------------------------------------------------------------------
        $scope.getItem = function (id) {
            studentsFormSvc.get(id).then(function (response) {
                $scope.saveText = "Update";
                $scope.isAdd = false;
                $scope.model = response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //---------------------------------------------delete studentsForm-------------------------------------------------------
        $scope.delete = function (id) {
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                studentsFormSvc.remove(id).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllstudentsForms();
                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //-------------------------------------------------------------------------------------------------------------------
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newItem = function () {
            $scope.redefineData();
        }
        //-------------------------------------------------------------------------------------------------------------------

        $scope.redefineData = function () {
            $scope.isAdd = true;
            $scope.saveText = "Save";
            $scope.model = {
                id: 0
            }
        }
        $scope.redefineData();

    }
    var module = angular.module("AcademicModule");
    module.controller("studentsFormController", ["$rootScope", "$scope", "$routeParams", "$route", "$ngBootbox", "Notification", "studentsFormSvc", studentsFormController]);
}());