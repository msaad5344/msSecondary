﻿(function () {

    var dashBoardSvc = function ($http, $q) {

        var getAll = function () {
             
            var deferred = $q.defer();
            
            $http.get("api/GetDashBoard").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });

             
            return deferred.promise;
        };
        
        return {
            getAll: getAll
        }


    };

    var module = angular.module("AcademicModule");
    module.factory("dashBoardSvc", ["$http", "$q", dashBoardSvc]);

}());