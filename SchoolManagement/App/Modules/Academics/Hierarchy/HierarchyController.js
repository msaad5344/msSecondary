﻿(function () {

    var hierarchyController = function ($rootScope, $scope, $routeParams, $route, $ngBootbox, Notification, hierarchySvc) {

        //------------------------------------------------------------------------------
          $scope.pageIncludes="";

        //---------------------------tree view colups----------------------------------------------------------------------------------------
        $scope.showStates = function (item) {
            item.active = !item.active;
        };
        $scope.showgets = function (item) {
            if (item.active)
                return "fa-minus-circle";
            else
                return "fa-plus-circle";
        };
      
     
        //-----------------initializing the list data-------------------------------------------------------------------------
        $scope.getAllhierarchy = function () {
            hierarchySvc.getAll().then(function (data) {
                $scope.items = data.data;
                // console.log(data);
               // alert("hi");
            }, function (error) {
                console.log(error.data);
            });
        }
        $scope.getAllhierarchy();
        //-------------------------------------------------------------------------------------------------------------------
        //--------------------------- client mangement----------------------------------------------------------------------------------------
      
      $scope.clienttapShow = function (id) {
           $scope.pageIncludes = "";
            var url = "/api/Hierarchy/Get/" + id;
            hierarchySvc.get(url)
                .then(function (response) {
                    //console.log(response);
                $scope.saveText = "Update";
                $scope.isAdd = false;
                $scope.isupdate = true;
                 $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/client.html";
                $scope.model = response.data;
            //    $scope.model.clt_logo = "/" + $scope.model.clt_logo;
               
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }

        $scope.savePhotoclient = function (photo) {
            myImg = {
                id: $scope.model.clt_id,
                myFile: photo[0]
            }
            var url="/api/Hierarchy/SaveClientPhoto/";
            hierarchySvc.saveImg(myImg,url).then(function (response) {
                $scope.model.clt_logo =response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newClient = function () {
            $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/client.html";
            $scope.isAdd = true;
            $scope.isupdate = false;
            $scope.saveText = "Save";
            $scope.model = {
                clt_id: 0
            }
        }
     

           //-----------------this function is for both update and create--------------------------------------------------------
        $scope.saveClient = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
            var url="/api/Hierarchy/Post";
                hierarchySvc.add($scope.model,url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تم الحفظ");
                        $scope.getAllhierarchy();
                        $scope.newClient();
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else{
            var url="/api/Hierarchy/Update";
                hierarchySvc.update($scope.model,url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllhierarchy();
                        $scope.newClient();
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }
        }
        //-------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------delete hierarchy-------------------------------------------------------
        $scope.deleteClientDataById = function (id) {
        var url="/api/Hierarchy/Delete/" + id;
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                hierarchySvc.remove(url).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllhierarchy();
                }, function (response) {
                    var status = response.status;
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //----------------------- end client mangement--------------------------------------------------------------------------------------------
    
           //--------------------------- entity mangement----------------------------------------------------------------------------------------
      
      $scope.entitytapShow = function (id) {
           $scope.pageIncludes = "";
            var url = "/api/Entity/Get/" + id;
            hierarchySvc.get(url)
                .then(function (response) {
                    //console.log(response);
                $scope.saveText = "Update";
                $scope.isAdd = false;
                $scope.isupdate = true;
                $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/entity.html";
                $scope.model = response.data;
            //    $scope.model.ent_logo = "/" + $scope.model.ent_logo;
               
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }

        $scope.savePhotoentity = function (photo) {
            myImg = {
                id: $scope.model.ent_id,
                myFile: photo[0]
            }
            var url = "/api/Entity/SaveEntityPhoto/";
            hierarchySvc.saveImg(myImg,url).then(function (response) {
                $scope.model.ent_logo =response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newEntity = function (clt_id,clt_code) {
            $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/entity.html";
            $scope.isAdd = true;
            $scope.isupdate = false;
            $scope.saveText = "Save";
            $scope.model = {
                ent_id: 0,
                clt_code: clt_code,
                clt_id: clt_id
            }
        }
     

           //-----------------this function is for both update and create--------------------------------------------------------
        $scope.saveentity = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
            var url="/api/Entity/Post";
                hierarchySvc.add($scope.model,url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تم الحفظ");
                        $scope.getAllhierarchy();
                        $scope.newEntity(data.clt_id, data.clt_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else{
            var url="/api/Entity/Update";
                hierarchySvc.update($scope.model,url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllhierarchy();
                        $scope.newEntity(data.clt_id, data.clt_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }
        }
        //-------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------delete Entity-------------------------------------------------------
        $scope.deleteentityDataById = function (id) {
        var url="/api/Entity/Delete/" + id;
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                hierarchySvc.remove(url).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllhierarchy();
                }, function (response) {
                    var status = response.status;
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //----------------------- end entity mangement--------------------------------------------------------------------------------------------
    
        //--------------------------- divission mangement----------------------------------------------------------------------------------------

        $scope.divissiontapShow = function (id) {
            $scope.pageIncludes = "";
            var url = "/api/Divission/Get/" + id;
            hierarchySvc.get(url)
                .then(function (response) {
                    //console.log(response);
                    $scope.saveText = "Update";
                    $scope.isAdd = false;
                    $scope.isupdate = true;
                    $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/divission.html";
                    $scope.model = response.data;
                    //    $scope.model.ent_logo = "/" + $scope.model.ent_logo;

                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
        }

        $scope.savePhotodivission = function (photo) {
            myImg = {
                id: $scope.model.div_id,
                myFile: photo[0]
            }
            var url = "/api/Divission/SaveDivisionPhoto/";
            hierarchySvc.saveImg(myImg, url).then(function (response) {
                $scope.model.div_logo = response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newDivission = function (ent_id, ent_code) {
            $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/divission.html";
            $scope.isAdd = true;
            $scope.isupdate = false;
            $scope.saveText = "Save";
            $scope.model = {
                div_id: 0,
                ent_code: ent_code,
                ent_id: ent_id
            }
        }


        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.saveDivission = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                var url = "/api/Divission/Post";
                hierarchySvc.add($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تم الحفظ");
                        $scope.getAllhierarchy();
                        $scope.newDivission(data.ent_id, data.ent_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else {
                var url = "/api/Divission/Update";
                hierarchySvc.update($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllhierarchy();
                        $scope.newDivission(data.ent_id, data.ent_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
        }
        //-------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------delete divission-------------------------------------------------------
        $scope.deleteDivissionDataById = function (id) {
            var url = "/api/Divission/Delete/" + id;
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                hierarchySvc.remove(url).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllhierarchy();
                }, function (response) {
                    var status = response.status;
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //----------------------- end divission mangement--------------------------------------------------------------------------------------------


        //--------------------------- courses mangement----------------------------------------------------------------------------------------

        $scope.coursestapShow = function (id) {
            $scope.pageIncludes = "";
            var url = "/api/Courses/Get/" + id;
            hierarchySvc.get(url)
                .then(function (response) {
                    //console.log(response);
                    $scope.saveText = "Update";
                    $scope.isAdd = false;
                    $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/courses.html";
                    $scope.model = response.data;
                    //    $scope.model.ent_logo = "/" + $scope.model.ent_logo;

                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
        }

        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newCourses = function (div_code) {
            $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/courses.html";
            $scope.isAdd = true;
            $scope.saveText = "Save";
            $scope.model = {
                crs_id: 0,
                div_code: div_code
            }
        }


        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.saveCourses = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                var url = "/api/Courses/Post";
                hierarchySvc.add($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تم الحفظ");
                        $scope.getAllhierarchy();
                        $scope.newCourses(data.div_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else {
                var url = "/api/Courses/Update";
                hierarchySvc.update($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllhierarchy();
                        $scope.newCourses(data.div_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
        }
        //-------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------delete courses-------------------------------------------------------
        $scope.deleteCoursesDataById = function (id) {
            var url = "/api/Courses/Delete/" + id;
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                hierarchySvc.remove(url).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllhierarchy();
                }, function (response) {
                    var status = response.status;
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //----------------------- end courses mangement--------------------------------------------------------------------------------------------



        //--------------------------- Baches mangement----------------------------------------------------------------------------------------

        $scope.BachestapShow = function (id) {
            $scope.pageIncludes = "";
            var url = "/api/Baches/Get/" + id;
            hierarchySvc.get(url)
                .then(function (response) {
                    //console.log(response);
                    $scope.saveText = "Update";
                    $scope.isAdd = false;
                    $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/baches.html";
                    $scope.model = response.data;
                    //    $scope.model.ent_logo = "/" + $scope.model.ent_logo;

                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
        }

        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newBaches = function (crs_id, crs_code) {
            $scope.pageIncludes = "/app/Modules/Academics/Hierarchy/baches.html";
            $scope.isAdd = true;
            $scope.saveText = "Save";
            $scope.model = {
                btc_id: 0,
                crs_id: crs_id,
                crs_code: crs_code
            }
        }


        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.saveBaches = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                var url = "/api/Baches/Post";
                hierarchySvc.add($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تم الحفظ");
                        $scope.getAllhierarchy();
                        $scope.newBaches(data.crs_id, data.crs_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else {
                var url = "/api/Baches/Update";
                hierarchySvc.update($scope.model, url).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAllhierarchy();
                        $scope.newBaches(data.crs_id, data.crs_code);
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
        }
        //-------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------delete Baches-------------------------------------------------------
        $scope.deleteBachesDataById = function (id) {
            var url = "/api/Baches/Delete/" + id;
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                hierarchySvc.remove(url).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAllhierarchy();
                }, function (response) {
                    var status = response.status;
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //----------------------- end Baches mangement--------------------------------------------------------------------------------------------

    }
    var module = angular.module("AcademicModule");
    module.controller("hierarchyController", ["$rootScope", "$scope", "$routeParams", "$route", "$ngBootbox", "Notification", "hierarchySvc", hierarchyController]);
}());