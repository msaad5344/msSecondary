﻿(function () {

    var hierarchySvc = function ($http, $q) {

        var getAll = function () {
            var deferred = $q.defer();
            $http.get("api/Hierarchy/GetAll").then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };
 

        var get = function (url) {
            var deferred = $q.defer();
            $http.get(url).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        
        var add = function (model,url) {
            var deferred = $q.defer();
            $http.post(url, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var update = function (model,url) {
            var deferred = $q.defer();
            $http.put(url, model).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var remove = function (url) {
            var deferred = $q.defer();
            $http.delete(url).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        };

        var saveImg = function (myImg,url) {
            var file = myImg.myFile;
            var id = myImg.id;
            var fm = new FormData();
            fm.append("file", file);
            fm.append("id", id);
            var deferred = $q.defer();
            $http.post(url, fm, {
                headers: { 'Content-Type': undefined }, transformRequest: angular.identity
            }).then(function (successResponse) {
                deferred.resolve(successResponse);
            }, function (failureResponse) {
                deferred.reject(failureResponse);
            });
            return deferred.promise;
        }

        return {
            getAll: getAll,
            get: get,
            add: add,
            update: update,
            remove: remove,
            saveImg :saveImg
        }
    };

    var module = angular.module("AcademicModule");
    module.factory("hierarchySvc", ["$http", "$q", hierarchySvc]);

}());