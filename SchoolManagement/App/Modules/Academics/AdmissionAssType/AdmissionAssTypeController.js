﻿(function () {

    var admissionAssTypeController = function ($rootScope, $scope, $routeParams, $route, $ngBootbox, Notification, admissionAssTypeSvc) {

        //-----------------Mohamed Saad Comment---Don't remove it-------------------------------------------------------------

        //thease comments is for the design pattern of separating the ListView from the Add and Update
        //put Your Global varaibles here
        //examble ==> $scope.data = [];

        //-------------------------------------------------------------------------------------------------------------------

        //-----------------this function is for both update and create--------------------------------------------------------
        $scope.save = function (form) {
            if (!form.$valid) {
                Notification.error("برجاء ملئ باقي البيانات اولا");
                return;
            }
            if ($scope.isAdd) {
                admissionAssTypeSvc.add($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        $scope.model = {
                        };
                        Notification.success("تم الحفظ");
                        $scope.getAlladmissionAssTypes();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }
                }, function (response) {
                    var status = response.status;
                    var data = response.data;

                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
            }
            else
                admissionAssTypeSvc.update($scope.model).then(function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 200) {
                        Notification.success("تمت العملية بنجاح");
                        $scope.getAlladmissionAssTypes();
                        $scope.redefineData();
                        form.$setUntouched();
                        form.$setPristine();
                    }

                }, function (response) {
                    var status = response.status;
                    var data = response.data;
                    if (status === 409) {
                        Notification.error(response.data);
                    }
                    else if (status === 500)
                        alert(JSON.stringify(data));
                });
        }
        //-------------------------------------------------------------------------------------------------------------------

        //-----------------initializing the list data-------------------------------------------------------------------------
        $scope.getAlladmissionAssTypes = function () {
            admissionAssTypeSvc.getAll().then(function (data) {
                $scope.admissionAssTypesList = data.data;
                console.log(data);
            }, function (error) {
                console.log(error.data);
            });
        }
        $scope.getAlladmissionAssTypes();
        //-------------------------------------------------------------------------------------------------------------------
        $scope.getItem = function (id) {
            admissionAssTypeSvc.get(id).then(function (response) {
                $scope.saveText = "Update";
                $scope.isAdd = false;
                $scope.model = response.data;
            }, function (response) {
                if (status === 500)
                    alert(JSON.stringify(data));
            });
        }
        //---------------------------------------------delete admissionAssType-------------------------------------------------------
        $scope.delete = function (id) {
            $ngBootbox.confirm('هل انت متاكد ؟ ').then(function () {
                admissionAssTypeSvc.remove(id).then(function (response) {
                    Notification.success('تم المسح بنجاح');
                    $scope.getAlladmissionAssTypes();
                }, function (response) {
                    if (status === 500)
                        alert(JSON.stringify(data));
                });
            }, function () {
            });
        };
        //-------------------------------------------------------------------------------------------------------------------
        //------------------------------------------redefine the model -------------------------------------------------------
        $scope.newItem = function () {
            $scope.redefineData();
        }
        //-------------------------------------------------------------------------------------------------------------------

        $scope.redefineData = function () {
            $scope.isAdd = true;
            $scope.saveText = "Save";
            $scope.model = {
                id: 0
            }
        }
        $scope.redefineData();

    }
    var module = angular.module("AcademicModule");
    module.controller("admissionAssTypeController", ["$rootScope", "$scope", "$routeParams", "$route", "$ngBootbox", "Notification", "admissionAssTypeSvc", admissionAssTypeController]);
}());