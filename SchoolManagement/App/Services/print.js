﻿(function () {

    var print = function ($http) {

        function getPdf(uri, params) {
            return $http({
                method: 'GET',
                url: uri,
                params: params,
                responseType: 'arraybuffer'
            });
        }

        function postPdf(uri, params) {
            return $http({
                method: 'POST',
                url: uri,
                data: params,
                responseType: 'arraybuffer'
            });
        }

        return {
            getPdf: getPdf,
            postPdf:postPdf
        }

    };

    var module = angular.module("App");
    module.service("print", ["$http", print]);

}());