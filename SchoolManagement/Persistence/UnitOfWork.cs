﻿using SchoolManagement.Models;
using SchoolManagement.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SchoolManagement.Persistence
{
    public class UnitOfWork
    {
        private readonly school_admission_dbEntities _context;
        private DbContextTransaction _transaction;
        public UnitOfWork()
        {
            _context = new school_admission_dbEntities();
        }
        private StudentForms _studentsform;
        public StudentForms StudentsForms => _studentsform ?? (_studentsform = new StudentForms(_context));

        private AdminAssStatus _adminAssStatus;
        public AdminAssStatus AdminAssStatus => _adminAssStatus ?? (_adminAssStatus = new AdminAssStatus(_context));


        private AdmissionAssType _admissionAssType;
        public AdmissionAssType AdmissionAssType => _admissionAssType ?? (_admissionAssType = new AdmissionAssType(_context));

        private AdmissionCategorys _admissionCategory;
        public AdmissionCategorys AdmissionCategory => _admissionCategory ?? (_admissionCategory = new AdmissionCategorys(_context));


        private Baches _baches;
        public Baches Baches => _baches ?? (_baches = new Baches(_context));


        private Clients _clients;
        public Clients Clients => _clients ?? (_clients = new Clients(_context));


        private Courses _courses;
        public Courses Courses => _courses ?? (_courses = new Courses(_context));


        private Divisions _divisions;
        public Divisions Divisions => _divisions ?? (_divisions = new Divisions(_context));


        private Entitys _entitys;
        public Entitys Entitys => _entitys ?? (_entitys = new Entitys(_context));


        private Sections _sections;
        public Sections Sections => _sections ?? (_sections = new Sections(_context));


        public int Complete()
        {
            int status = _context.SaveChanges();
            return status;
        }



        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            _transaction = _context.Database.BeginTransaction(isolationLevel);
        }


        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }


        public void Dispose()
        {
            _context.Dispose();
        }
    }
}