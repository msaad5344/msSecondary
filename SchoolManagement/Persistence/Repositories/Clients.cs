﻿using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SchoolManagement.Persistence.Repositories
{
    public class Clients : Repository<Client>
    {
        public Clients(DbContext context) : base(context)
        {
        }
    }
}